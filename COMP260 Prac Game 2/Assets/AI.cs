﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class AI : MonoBehaviour
{
    public GameObject Puck;


    private Rigidbody rigidbody;
    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;

    }

    private float force = 10f;
    void Update()
    {
        if (Puck.transform.position.x >= transform.position.x)
        {

            transform.position = new Vector3(transform.position.x, 0f, transform.position.z + (0.2f * Time.deltaTime));
        }
        if (Puck.transform.position.x <= transform.position.x)
        {

            transform.position = new Vector3(transform.position.x, 0f, transform.position.z - (0.2f * Time.deltaTime));
        }

        Vector3 dir = Puck.transform.position - rigidbody.position;
        rigidbody.AddForce(Puck.transform.position * force);



    }
}

