﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

    public AudioClip scoreClip;
    private AudioSource audio;

    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {

    }

    public int player; // who gets points for scoring in this goal
    public int player2;

  

    void OnTriggerEnter(Collider collider)

    {
        
        // play score sound
        audio.PlayOneShot(scoreClip);

        // tell the scorekeeper
        Scorekeeper.Instance.OnScoreGoal(player);
        Scorekeeper.Instance.OnScoreGoal(player2);

        PuckControl Puck =
            collider.gameObject.GetComponent<PuckControl>();
        Puck.ResetPosition();


    }
}
