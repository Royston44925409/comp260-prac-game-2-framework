﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Scorekeeper : MonoBehaviour {

    public int pointsPerGoal = 1;
    private int[] score = new int[2];
    public Text[] scoreText;

    static private Scorekeeper instance;

    static public Scorekeeper Instance
    {
        get { return instance; }
    }

    // Use this for initialization
    void Start () {

        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError(
            "More than one Scorekeeper exists in the scene.");
        }

        // reset the scores to zero
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }
    }

    public void OnScoreGoal(int player1)
    {
        
        score[player1] += pointsPerGoal;
        scoreText[player1].text = score[player1].ToString();
        
     

    }
    
    // Update is called once per frame
    void Update () {
		
	}
}
